package com.purnima.jain.customer.domain.repository;

import java.util.List;

import com.purnima.jain.customer.domain.aggregate.Customer;

public interface CustomerRepository {

	public Customer getCustomerById(Integer customerId);

	List<Customer> getCustomerByAgeGreaterThan(Integer age);

}
