package com.purnima.jain.customer.repository.implementation;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import com.purnima.jain.customer.domain.aggregate.Customer;
import com.purnima.jain.customer.domain.repository.CustomerRepository;

@Profile("jpa")
@Repository
public class CustomerRepositoryJpaImpl implements CustomerRepository{

	
	@PersistenceContext
    private EntityManager entityManager;
	
	public Customer getCustomerById(Integer customerId) {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<Customer> cq = cb.createQuery(Customer.class);
		Root<Customer> model = cq.from(Customer.class);
		cq.where(cb.equal(model.get("id").as(Long.class), customerId));
		return entityManager.createQuery(cq).getSingleResult();
	}
	
	@Override
	public List<Customer> getCustomerByAgeGreaterThan(Integer age) {
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Customer> query = builder.createQuery(Customer.class);
		Root<Customer> c = query.from(Customer.class);
		query.select(c).where(builder.greaterThan(c.get("age").as(int.class), 25));
		return entityManager.createQuery(query).getResultList();
	}
}
